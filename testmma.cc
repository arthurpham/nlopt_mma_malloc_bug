#include <nlopt.hpp>

#include <vector>
#include <cmath>
#include <iostream>


namespace{
	double myfunc(unsigned n, const double *x, double *grad, void *my_func_data)
	{
		if (grad) {
			grad[0] = 0.0;
			grad[1] = 0.5 / std::sqrt(x[1]);
		}
		return std::sqrt(x[1]);
	}

	typedef struct {
		double a, b;
	} my_constraint_data;

	double myconstraint(unsigned n, const double *x, double *grad, void *data)
	{
		my_constraint_data *d = (my_constraint_data *)data;
		double a = d->a, b = d->b;
		if (grad) {
			grad[0] = 3 * a * (a*x[0] + b) * (a*x[0] + b);
			grad[1] = -1.0;
		}
		return ((a*x[0] + b) * (a*x[0] + b) * (a*x[0] + b) - x[1]);
	}

    void testnlopt()
    {
        // from https://nlopt.readthedocs.io/en/latest/NLopt_Tutorial/#example-in-c

        nlopt::opt opt(nlopt::LD_MMA, 2);
        std::vector<double> lb(2);
        lb[0] = -HUGE_VAL; lb[1] = 0;
        opt.set_lower_bounds(lb);
        opt.set_min_objective(myfunc, NULL);
        //opt.set_param("verbosity", 1);

        my_constraint_data data[2] = { { 2, 0 }, { -1, 1 } };
        opt.add_inequality_constraint(myconstraint, &data[0], 1e-8);
        opt.add_inequality_constraint(myconstraint, &data[1], 1e-8);
        opt.set_xtol_rel(1e-4);
        std::vector<double> x(2);
        x[0] = 1.234; x[1] = 5.678;
        double minf = HUGE_VAL;

        nlopt::result result = opt.optimize(x, minf);

        // The result of running the program should then be something like:
        // found minimum at f(0.333334,0.296296) = 0.544330847
        std::cout << "x[0] " << x[0] << std::endl;
        std::cout << "x[1] " << x[1] << std::endl;
        std::cout << "minf " << minf << std::endl;
    }
}

int main(int argc, char *argv[])
{
    testnlopt();
}
